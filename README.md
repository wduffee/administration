# Open Tennessee
Open Tennessee is a nonprofit coming in 2020.

See www.codefornashville.org for an idea of what we will be (with some important organizational and diversity, equity, and inclusion improvements).

We advocate for more:

- Open Data
- Open Source (Code)
- Open Community
- Open Government

Please reach out to https://twitter.com/combinatorist or `tim` at codefornashville.org if you have concerns, ideas, or an interest in shaping this new organization.

We will develop this organization as transparently as possible and this git repository will hold our founding documents and policies.
